Reportes
************

Reportes de Campañas
=====================

Esta sección permite evaluar la efectividad de las campañas, siendo una herramienta útil en la toma de decisiones relacionadas con la realización de nuevas campañas. 
La herramienta permite hacer un seguimiento sobre los correos abiertos, pudiendo determinar cuántos correos se han abierto abierto, que cantidad de veces, al igual que permite determinar el número de enlaces dentro del cuerpo de un mensaje se ha abierto y la cantidad de veces que se ha abierto
  * campañas enviadas 
  * porcentaje de emails abiertos
  * cuantas veces se ha abierto un mensaje (total por campaña).
  * porcentaje enlaces abiertos
  * cantidad de veces que se ha abierto un enlace

Para acceder a esta sección ingrese al menú *Estadísticas* en la barra lateral

 .. image:: img/manual/menu/3.png

seleccione posteriormente la opción *Campañas*

 .. image:: img/manual/menu/4.png


Datos para entender los reportes
----------------------------------------------

Los reportes están asociados a una única campaña.

Tenga en cuenta la siguiente leyenda de color:

 * elemento enviado:

  .. image:: img/manual/reportes/1.png 

 * elemento abierto:

  .. image:: img/manual/reportes/2.png 

Además tenga en cuenta:

 * **número de abiertos:** es igual al total de veces en abrirse un elemento.
 * **clic únicos:** solo cuenta una apertura por elemento. 


Reportes disponibles
---------------------

Nivel de actividad
^^^^^^^^^^^^^^^^^^^

Permite conocer el nivel de actividad en cuanto a la apertura de correos y sus enlaces

  .. image:: img/manual/reportes/3.png   


Lugar de apertura
^^^^^^^^^^^^^^^^^^

Permite conocer la distribución geográfica de apertura de los correos. Incluye información nombre lugar y cantidad total de abiertos, y resumen de total por lugar. Cuenta con mapas de calor, los marcadores del mapa tienen un determinado color según el número de correos abiertos, así como la gráfica de distribución.

Permite conocer la distribución geográfica de apertura de los correos. Incluye información nombre lugar, cantidad total de abiertos, y resumen de total por lugar. Cuenta con mapas de calor, los marcadores del mapa tienen un determinado tamaño según el número de correos abiertos, así como la gráfica de distribución tienen un determinado color según el número de correos abiertos por locación.

  .. image:: img/manual/reportes/4.png 


Horario de apertura de los correos
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Permite visualizar el horario preferido de apertura de los correos por parte de los suscriptores. 

  .. image:: img/manual/reportes/5.png 

Aperturas de la campaña
^^^^^^^^^^^^^^^^^^^^^^^^

Permite visualizar el listado de todas las aperturas realizadas en la campaña por cada suscriptor.

  .. image:: img/manual/reportes/9.png 


Actividad de clics hechos sobre enlaces
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  .. image:: img/manual/reportes/6.png 

puede descargar el reporte en formato csv presionando el control de la parte superior derecha  

  .. image:: img/manual/reportes/6b.png 

Últimos correos rebotados
^^^^^^^^^^^^^^^^^^^^^^^^^^

Tabla con los n últimos correos rebotados por estado, fecha , razón (8 razones posibles). Este listado puede descargarse en formato csv.

  .. image:: img/manual/reportes/7.png 


Clientes de correo preferido
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  .. image:: img/manual/reportes/8.png 

puede visualizar información adicional de este reporte presionando el control de la parte superior derecha

  .. image:: img/manual/reportes/8b.png

**tabla de cicks:**

  .. image:: img/manual/reportes/8c.png 

**mapa de cicks:**

  .. image:: img/manual/reportes/8d.png 






