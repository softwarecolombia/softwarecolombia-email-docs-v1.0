Campañas
********

Esta sección permite crear campañas y consultar el listado de campañas creadas junto con información de su estado.
Una campaña consiste en un único envió de correo electrónico a N listas de suscripción o a N sublistas de suscriptores (segmentos), por tanto la campaña tiene un ciclo de vida corto.

Una campaña consta de:
 * asunto
 * destinatarios (listas o listado de segmentos)
 * cuerpo  formato de html o texto plano (contenido del mensaje) 
 * fecha de envío

Para acceder a esta sección ingrese al menú *Campañas* en la barra lateral

 .. image:: img/manual/menu/2.png

Prerrequisitos
==============

.. warning::  Antes de poder realizar el envío de campañas, es necesario efectuar alguna de las siguientes opciones:
  
  * **Registrar un dominio:** Luego de registrado permitirá enviar correos electrónicos cuyo remitente es un correo electrónico de ese dominio.

  * **Registrar un remitente:**  Luego de registrado permitirá enviar correos electrónicos cuyo remitente es la dirección registrada.

  Para realizar el registro debe contactarse con Software Colombia. Tenga en cuenta que registrar un remitente es una operación mucho más sencilla que realizar el registro del dominio por lo que tardará menos tiempo en efectuarse.


Listado de campañas
=========================

Cuando se ingresa a la sección de campañas, se pueden visualizar las listas de campañas creadas en la herramienta. Las campañas son listadas de la más nueva a la más antigüa. Para cada campaña se puede visualizar:

 * nombre de la campaña en formato ``[Nombre de la campaña] - [Nombre de la lista / sub-lista]``
 * fecha de envío de la campaña
 * cantidad de suscriptores
 * cantidad de correos abiertos
 * cantidad de enlaces abiertos
 * control de herramientas de campaña 

.. image:: img/manual/campana/2.png

Herramientas Campañas
-------------------------------

Al presionar el control de herramientas asociado a una campaña se desplegan las siguientes opciones:

.. image:: img/manual/campana/tools/1.png 


Ver suscriptores
^^^^^^^^^^^^^^^^

.. image:: img/manual/campana/tools/2.png 

Permite visualizar los suscriptores asociados a la campaña.


.. image:: img/manual/campana/tools/3.png 

Note que es dirigido a la sección de suscriptores.

Duplicar campaña
^^^^^^^^^^^^^^^^

.. image:: img/manual/campana/tools/5.png 

Permite duplicar la configuración de una campaña.

.. image:: img/manual/campana/tools/6.png 

La nueva campaña aún no tiene una fecha de envió creada y puede hacerle las modificaciones que sean necesarias.

Borrar campaña
^^^^^^^^^^^^^^^^

.. image:: img/manual/campana/tools/8.png 

Permite borrar una campaña. Antes de poder borrar la campaña debe confirmar que desea removerla.

.. image:: img/manual/campana/tools/9.png

Una vez realice la confirmación un mensaje informativo será desplegado. 

.. image:: img/manual/campana/tools/10.png 

Filtrar campañas
-------------------------------

Las campañas pueden ser filtradas utilizando los siguientes controles:

**Paginación:** Permite navegar las distintas páginas de campañas. Muy util para acceder a campañas viejas.  

.. image:: img/manual/campana/fl_2.png

**Filtros de estado:** Permiten filtrar la lista según el estado de las campañas, agrupadas o todas. Por defecto está seleccionada la opción Todas.

.. image:: img/manual/campana/fl_1.png

Si la opción *Todas* es seleccionada se listan todas las campañas en el formato  ``[Nombre de la campaña] - [Nombre de la lista / sub-lista]``, es decir incluyendo todos las listas y/o sublistas(segmentos) a los que se envío la campaña. 

.. image:: img/manual/campana/fl_4.png

Por el contrario si la opción *Agrupadas* es seleccionada se listan todas las campañas en el formato  ``[Nombre de la campaña] - [Nombre de la lista]``, es decir se consolidan todos los segmentos o sublistas  asociados a  una campaña. 

.. image:: img/manual/campana/fl_3.png

.. note:: Cuando se envía una campaña a múltiples listas en realidad se crean varias campañas con el mismo contenido pero distintos destinatarios, por tanto en las páginas de lista de reportes y la lista de campañas se pueden visualizar todas las campañas que hay. Si una campaña se envio a 5 listas de suscripción y esta activo el filtro *Todas* se verán 5 campañas, si esta activo el filtro *Agrupadas* solo se verá una campaña.

Crear campaña
===================

Al presionar el botón *Nueva campaña* se despliega el siguiente formulario:

Elementos de la campaña
--------------------------------

.. image:: img/manual/campana/4.png 

Cada campo se debe diligenciar así:

 * **Nombre:** Nombre de la campaña
 * **Nombre remitente:** Nombre que debe aparecer como remitente en el correo electrónico
 * **Asunto del correo:** Asunto del correo electrónico a enviar.
 * **Dirección de correo remitente:** Dirección de correo electrónico que debe aparecer como remitente en el correo electrónico a enviar
 * **Destinatarios:** Lista de suscriptores a la cual realizar el envió. este campo es de tipo lista desplegable. 

.. image:: img/manual/campana/5.png 

Una vez diligenciados estos campos presione  

.. image:: img/manual/campana/6.png 

para avanzar.

Selección de plantilla
-------------------------------

La herramienta permite elegir entre plantillas prediseñadas o plantillas por código:

.. image:: img/manual/campana/7.png 

Plantillas prediseñadas
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Plantillas previamente cargadas en el sistema. Simplemente se elige la plantilla a usar.

Plantillas por código
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: img/manual/campana/8.png 

Las plantillas por código pueden ser de tres tipos:

* **Escribe codigo:** Te permite escribir directamente el código html del mensaje.

  .. image:: img/manual/campana/9.png

  Es posible guardar cambios temporales y previsualizar el mensaje presionando 

  .. image:: img/manual/campana/10.png

  .. image:: img/manual/campana/11.png  

  o  puede almacenarlo para modificarlo más tarde

  .. image:: img/manual/campana/12.png 
 
  Una vez seleccionada la plantilla

  .. image:: img/manual/campana/13.png 



*  **Escribe el correo:** Permite redactar en el mensaje en un editor de tipo WYSIWYG (lo que ve, es lo que escribe).

  .. image:: img/manual/campana/14.png 

  Puede programar el envío de la campaña

  .. image:: img/manual/campana/15.png 


  o enviarla directamente ,


  .. image:: img/manual/campana/16.png



* **Sube codigo:**
  Permite subir una plantilla en formato zip. El contenido del zip debe ser un archivo html.

  .. image:: img/manual/campana/17.png 

  .. image:: img/manual/campana/18.png 

  .. image:: img/manual/campana/19.png 



Para programar la campaña es necesario configurar la fecha y hora de envío.


  .. image:: img/manual/campana/20.png 

  .. image:: img/manual/campana/21.png 

  .. image:: img/manual/campana/22.png 

Y presione

  .. image:: img/manual/campana/23.png 


La campaña quedará almacenada como borrador. Puede darse cuenta porque el nombre de la campaña es de color gris y tiene la palabra DRAFT concatenada al final de su nombre. 

.. image:: img/manual/campana/24.png 

Una vez enviada la campaña el color del nombre de la campaña es de color verde y puede visualizar el número de suscriptores de la campaña, número de correos electrónicos y enlaces abiertos. 

.. image:: img/manual/campana/25.png 

