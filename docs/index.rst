.. Read the Docs Template documentation master file, created by
   sphinx-quickstart on Tue Aug 26 14:19:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a la documentación de  eMail!
==================================================

.. Contenido:

.. toctree::
   :maxdepth: 5
   
   ventajas
   requisitos         
   manual
   preguntas
   glosario
   history




.. Indices y tablas
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

