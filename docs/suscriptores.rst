Suscriptores
************

Esta sección es muy útil porque permite la segmentación de suscriptores. Las principales tareas que permite realizar son:
 * crear listas y consultar el listado de las listas de contactos creadas junto con información de sus integrantes
 * importar suscriptores a una lista
 * modificar la información básica de una lista
 * exportar listas
 * eliminar listas
 * editar las etiquetas de los atributos de los suscriptores las cuales son un insumo esencial para permitir la creación de mensajes de correos electrónicos personalizados

Para acceder a esta sección ingrese al menú *Suscriptores* en la barra lateral

.. image:: img/manual/menu/1.png

Consultar las listas de suscriptores
=====================================

Al ingresar a la sección de suscriptores se pueden visualizar las listas de suscriptores creadas en la herramienta junto con información general de cada lista como su fecha de creación, el número de contactos activos, retirados y limpiados.:

.. image:: img/manual/suscriptores/2.png


Atributos de suscriptores
===============================
Los atributos de los suscriptores es aquella Información que se guarda de cada suscriptor. Esta información puede ser utilizada posteriormente para la personalización de los mensajes. Los atributos almacenados por cada suscriptor varían de acuerdo a los datos capturados e importados a la plataforma sobre cada suscriptor, vía csv, xlsx o formulario embebido. Los más comunes son:
 * nombre
 * email
 * país
 * ciudad
 * teléfono
 * ocupación 

Crear lista de suscriptores
===========================
Al presionar el botón 

.. image:: img/manual/suscriptores/crear1.png 

se despliega el formulario

.. image:: img/manual/suscriptores/crear2.png 

la información que debe diligenciar en cada campo es:
 * **Nombre:** Nombre que tendra la lista
 * **Nombre por defecto del remitente:** Nombre que aparecerá como remitente de los correos electrónicos enviados a esta lista.
 * **Direccion del correo por defecto del remitente:** Dirección de correo electrónico que aparecerá como remitente de los correos electrónicos enviados a esta lista.

presione el botón *Guardar* para guardar

  .. image:: img/manual/suscriptores/crear3.png  

tan pronto como guarde podra visualizar el detalle de la lista de suscriptores recien creada.

 .. image:: img/manual/suscriptores/crear6.png  

como la lista acaba de ser creada no tiene suscritos.
 
Ver detalle de una lista de suscriptores
==========================================
Cuando se selecciona una de las listas de suscriptores se pueden visualizar todos los contactos que hacen parte de esa lista, algunos controles para filtrar y realizar búsquedas sobre los contactos de la lista y adicionalmente se puede acceder a herramientas que permiten interactuar con la lista, como editar su información, importar contactos, personalizar etiquetas y exportar la lista.

.. image:: img/manual/suscriptores/3.png

.. _filtrarsuscriptoressegmento-label:

Filtrar listas de suscriptores
-------------------------------

Los contactos de una lista de suscriptores pueden ser filtrados utilizando los siguientes controles:

**Segmento:** Permite filtrar la lista de contactos de acuerdo a un segmento previamente definido.  

.. image:: img/manual/suscriptores/f_1.png

al presionarlo se despliega un listado de todos los segmentos creados para esa lista.

.. image:: img/manual/suscriptores/f_2.png 

Elija un segmento para ver los suscriptores que hacen parte de este. Elija *Todos los suscriptores* para ver el listado completo.

**Filtros de estado:** Permiten filtrar la lista según el estado de los contactos, según sea suscrito, retirado o eliminado. Por defecto está seleccionada la opción Todos.

.. image:: img/manual/suscriptores/5.png

**Límite de resultados por página:** Permite configurar el número de contactos a visualizar en una página. Los posibles valores son 10, 25, 50 y 100.


.. image:: img/manual/suscriptores/6.png


**Cuadro de búsqueda:** Permite ingresar un término que esté incluido en el nombre o dirección de correo electrónico del contacto.  


.. image:: img/manual/suscriptores/7.png

**Paginación:** Permite navegar las distintas páginas de una lista de suscriptores. Cuando el número de contactos de la lista de suscriptores es superior al límite de resultados por página establecido se genera paginación.  


.. image:: img/manual/suscriptores/pag.png


Eliminar lista de suscriptores
------------------------------

Dentro del detalle de una lista de suscriptores seleccione el menú *Herramientas->Administrar suscriptores*

.. image:: img/manual/suscriptores/elim1.png

active el radiobutton con el texto *Deseo eliminar la lista XXX*

.. image:: img/manual/suscriptores/elim2.png

si presiona *Eliminar lista* se despliega una ventana emergente informando que la operación fue exitosa.

.. image:: img/manual/suscriptores/elim4.png

presione *Aceptar*. La ventana se ocultara y será dirigido a la vista con las listas de suscriptores.

.. image:: img/manual/suscriptores/elim6.png


Herramientas
-----------------------------------

Al presionar el botón *Herramientas* de la vista de detalle de una lista de suscriptores, se pueden observar las herramientas disponibles:

.. image:: img/manual/suscriptores/4.png


Actualizar información básica
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

En la vista de detalle de una lista de suscriptores ingrese al menú *Herramientas -> Actualizar información básica*

.. image:: img/manual/suscriptores/tools/act_1.png

se desplegara un formulario con los datos de la lista

.. image:: img/manual/suscriptores/tools/act_2.png

modifique los datos según sea necesario y guarde

.. image:: img/manual/suscriptores/tools/act_4.png

la información básica de la lista es actualizada. Será dirigido a la vista de detalle de la lista

.. image:: img/manual/suscriptores/tools/act_5.png


Importar una lista de suscriptores
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Hay dos maneras de importar contactos a las listas, una es mediante la carga de un archivo (csv o excel) o mediante un formulario de registro. Para importar por medio de archivo en la vista de detalle de una lista de suscriptores ingrese al menú *Herramientas -> Importar una lista de suscriptores*

.. image:: img/manual/suscriptores/8.png

decida qué tipo de archivo quiere importar

.. image:: img/manual/suscriptores/9.png

Importar desde un archivo csv
"""""""""""""""""""""""""""""

Cuando selecciona la opción de csv, se le desplegará una ventana de carga de archivos. Debe hacer clic sobre el icono de carga o arrastrar el archivo csv hasta esta zona.

.. image:: img/manual/suscriptores/10.png

.. image:: img/manual/suscriptores/11.png

una vez seleccionado el archivo es posible subir la lista.

.. image:: img/manual/suscriptores/12.png

al presionar el botón de subir lista se realiza el proceso de carga y se informa los resultados al usuario. Si alguno de los contactos se encuentra registrado previamente el sistema lo ignora. 

.. image:: img/manual/suscriptores/13.png

una vez se presione Aceptar, se es redirigido a la lista de suscriptores.


Importar desde un archivo xls/xlsx
""""""""""""""""""""""""""""""""""

Cuando selecciona la opción de xls/xlsx, se le desplegará una ventana de carga de archivos. Debe hacer clic sobre el icono de carga o arrastrar el archivo csv hasta esta zona.

.. image:: img/manual/suscriptores/14.png

una vez seleccionado el archivo es posible subir la lista.

.. image:: img/manual/suscriptores/15.png

al presionar el botón de subir lista se realiza el proceso de carga y se informa los resultados al usuario. Si alguno de los contactos se encuentra registrado previamente el sistema lo ignora. 

.. image:: img/manual/suscriptores/16.png

una vez se presione Aceptar, se es redirigido a la lista de suscriptores.

Importar desde formulario externo
"""""""""""""""""""""""""""""""""

Es posible embeber en su página web un formulario de registro a una lista de suscripción. Cada vez que un usuario se registre quedará inmediatamente agregado en la lista de contactos de esa lista.

.. image:: img/manual/suscriptores/17.png

Si requiere de un formulario embebible para algunas de sus listas de suscripción contactese con Software Colombia para generarlo.

Crear un segmento
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Esta opción permite segmentar una lista de suscriptores de acuerdo a como cumplan ciertos criterios personalizables, como que los contactos tengan cierto rango de edad, sean de  un género en específico, sean de cierta ciudad o profesión, etc. 

Ingrese al menú *Herramientas -> Crear un segmento*

.. image:: img/manual/suscriptores/tools/seg_0.png

Un segmento esta compuesto por un nombre, una descripción y una o más condiciones.
Una condición esta formada por un atributo de un suscriptor, un tipo de condición y un valor.

.. image:: img/manual/suscriptores/tools/seg_1.png

Para crear un segmento diligencie el formulario y presione el botón *Guardar*.

.. hint:: Puede agregar más condiciones presionando el botón *Agregar condición*.

Los tipos de condiciones existentes son:

.. image:: img/manual/suscriptores/tools/seg_6.png

el significado de cada una de estas condiciones es el siguiente:

 * **IS:** el valor de la propiedad es exactamente igual al valor ingresado (puede ser vacio).
 * **IS_NOT:** el valor de la propiedad es diferente al valor ingresado (puede ser vacio).
 * **CONTAINS:** el valor de la propiedad contiene el valor ingresado (puede ser vacio).
 * **DOES_NOT_CONTAINS:** el valor de la propiedad no contiene el valor ingresado (puede ser vacio).
 * **STARTS_WITH:** el valor de la propiedad empieza el valor ingresado.
 * **ENDS_WITH:** el valor de la propiedad termina el valor ingresado.
 * **IS_GREATER_THAN:** el valor de la propiedad es mayor al valor ingresado.
 * **IS_LESS_THAN:** el valor de la propiedad es menor al valor ingresado.
 * **IS_BLANK:** el valor de la propiedad es vacío.
 * **IS_NOT_BLANK:** el valor de la propiedad tiene algún valor.

 .. hint:: Si quiere que los contactos del segmento elegido cumplan todas las condiciones, active el control *Cada suscriptor debe cumplir todas las condiciones*. Por el contrario si basta con que se cumplan alguna de las condiciones para que un suscriptor quede en el segmento desactive este control.

Ejemplos
""""""""
Para los ejemplos a continuación se manejara el siguiente listado de contactos

.. image:: img/manual/suscriptores/tools/seg_7.png

**Segmento1: Todas las personas con apellido Rodríguez**

.. image:: img/manual/suscriptores/tools/seg_8.png

.. image:: img/manual/suscriptores/tools/seg_12.png

**Segmento2: Todas las personas con apellido Rodríguez y que no esten en la zona "uno"**

.. image:: img/manual/suscriptores/tools/seg_9.png

.. image:: img/manual/suscriptores/tools/seg_13.png

**Segmento3: Todas las personas con apellido Rodríguez o que estan en la zona "uno"**

.. image:: img/manual/suscriptores/tools/seg_10.png

.. image:: img/manual/suscriptores/tools/seg_14.png

**Segmento4: Todas las personas con un nivel de sensibilidad mayor a 4**

.. image:: img/manual/suscriptores/tools/seg_11.png

.. image:: img/manual/suscriptores/tools/seg_15.png


Ver suscriptores de un segmento
"""""""""""""""""""""""""""""""

Vea :ref:`Filtar suscriptores por segmento <filtrarsuscriptoressegmento-label>`.

Agregar un suscriptor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Ingrese al menú *Herramientas -> Agregar un suscriptor*

.. image:: img/manual/suscriptores/tools/Agr_1.png

se desplegara un formulario solicitandole la dirección de correo electrónico a agregar

.. image:: img/manual/suscriptores/tools/Agr_2.png

ingrese el dato solicitado y guarde

.. image:: img/manual/suscriptores/tools/Agr_3.png

se desplegará en la interfaz un mensaje de éxito

.. image:: img/manual/suscriptores/tools/Agr_4.png

sera dirigido a la página de detalle de la lista. La lista incluira el contacto recien agregado. 

.. image:: img/manual/suscriptores/tools/Agr_5.png

Si no puede encontrar el contacto en el listado desplegado verifique la páginación de la tabla, lo más seguro es que este en otra página.

Personalizar etiquetas
^^^^^^^^^^^^^^^^^^^^^^

Ingrese al menú *Herramientas -> Personalizar etiquetas*

Las etiquetas o tags permiten usar en el correo electrónico anotaciones con el formato ``${TAG’X’}``, cada anotación está relacionada con un único dato del suscriptor. Se usan para que al momento de crear el correo electrónico estás sean reemplazadas por el dato del usuario, es decir, si ``${TAG1}`` está relacionado con el nombre del suscriptor y el correo es: “Buenas tardes ``${TAG1}``”, entonces cuando se cree el correo ``${TAG1}`` será reemplazado por el nombre del suscriptor, dando como resultado algo similar a: ‘’Buenas tardes Cristian”, para el caso en que el suscriptor se llame Cristian.

.. image:: img/manual/suscriptores/18.png

Las etiquetas se crean al momento de subir una lista de suscriptores, adicionalmente el usuario puede crear todas las que quiera, pero solo UNA por cada dato del suscriptor, es decir una para el teléfono, una para el nombre etc. No puede haber etiquetas repetidas.

Se pueden crear nuevas etiquetas al adicionar nuevos contactos a una lista ya existente.

Editar suscriptores
^^^^^^^^^^^^^^^^^^^

Ingrese al menú *Herramientas -> Editar suscriptores*

.. image:: img/manual/suscriptores/tools/Edt_1.png

se desplegara el listado de suscriptores con un botón que permite modificar de manera individual a cada suscriptor. Al presionar este botón se despliega un formulario solicitando el nuevo correo electrónico del contacto.

.. image:: img/manual/suscriptores/tools/Edt_2.png

ingrese el dato solicitado y guarde

.. image:: img/manual/suscriptores/tools/Edt_3.png

se desplegará en la interfaz un mensaje de éxito

.. image:: img/manual/suscriptores/tools/Edt_4.png

sera dirigido a la página de detalle de la lista. La lista incluira el contacto recien agregado. 

.. image:: img/manual/suscriptores/tools/Edt_5.png

Si no puede encontrar el contacto en el listado desplegado verifique la páginación de la tabla, lo más seguro es que este en otra página.


Exportar suscriptores
^^^^^^^^^^^^^^^^^^^^^
Esta opción le permitira descargar la lista de suscriptores en formato csv. Ingrese al menú *Herramientas -> Exportar suscriptores*

.. image:: img/manual/suscriptores/tools/expt_1.png

seleccione la ubicación en la que desea almacenar el archivo y acepte la descarga.

.. image:: img/manual/suscriptores/tools/expt_2.png

el nombre del archivo de descarga sigue la siguiente convención Subscribers - NombreDeLaLista.csv

.. image:: img/manual/suscriptores/tools/expt_3.png


Administrar suscriptores
^^^^^^^^^^^^^^^^^^^^^^^^

Esta herramienta le permite eliminar una lista de suscriptores y su información relacionada. 

Dentro del detalle de una lista de suscriptores seleccione el menú *Herramientas->Administrar suscriptores*

.. image:: img/manual/suscriptores/elim1.png

active el radiobutton con el texto *Deseo eliminar la lista XXX*

.. image:: img/manual/suscriptores/elim2.png

si presiona *Eliminar lista* se despliega una ventana emergente informando que la operación fue exitosa.

.. image:: img/manual/suscriptores/elim4.png

presione *Aceptar*. La ventana se ocultara y será dirigido a la vista con las listas de suscriptores.

.. image:: img/manual/suscriptores/elim6.png

