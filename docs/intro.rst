Entrada al componente
*********************
Ingrese a la siguiente url

.. image:: img/manual/entrada/0.png

.. image:: img/manual/entrada/1.png

debe ingresar con las credenciales de acceso (nombre de usuario y contraseña) proporcionadas por Software Colombia. Una vez el usuario se autentica es dirigido por defecto a la sección de campañas. El sitio tiene la siguiente estructura:

.. image:: img/manual/entrada/2.png

* **Barra lateral:** Permite acceso a las principales secciones de la herramienta
     - Suscriptores
     - campañas
     - Reportes
* **Barra superior:** Tiene algunos controles para cambiar ligeramente la diagramación del sitio y para cerrar sesión
* **Contenido:** Sección donde se carga el contenido y menús contextuales que permiten interactuar con ese contenido
