Ventajas
========

eMail le ofrece:

- envío masivo de correos electrónicos
- garantía de despacho
- adherencia a reglas de privacidad
- compatible con múltiples lectores de correo y dispositivos móviles
- herramientas de segmentación de listas
- implementación de formulario online para suscripción a boletines desde formulario embebido en el sitio web.
- plantillas personalizables
- personalización de campos de listas
- programación de envío de correos en fechas posteriores
- generación de reportes e informes de entrega y apertura detallados



