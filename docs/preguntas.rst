Preguntas más frecuentes
========================

- **¿Cuántos contactos pueden ser agregados a una lista?**

  Depende de la parte contractual, límite de 20.000 contactos

- **Cuántas listas se pueden tener?**

  Depende de la parte contractual

- **¿Cual es la duración de la licencia?**

  Depende de la parte contractual. Por lo general se renueva de manera anual.

- **¿Cómo embebo un formulario de registro a una lista de distribución en mi página?**

  Para embeber un formulario se debe tener la url de este. Por ejemplo existe un formulario en la url: ``http://www.software-colombia.com/tmp/SCSubscriber/subscriberForm/CMHSubscriberForm.html`` Adicional de ello se debe tener en cuenta el campo hidden llamado “l”, que es el que contiene el id de la lista de suscripción. También hay dos campos hidden más con referencias a páginas web, una para respuesta correcta cuando el suscriptor se inscribió correctamente y otra para respuesta errada. el ejemplo de la versión embebida está en la web de centro memoria usando un iframe: 

  .. note:: <iframe src="http://www.software-colombia.com/tmp/SCSubscriber/subscriberForm/CMHSubscriberForm.html" scrolling="no" width="645" height="900"></iframe>

- **¿Se puede eliminar una campaña?**
  Por el momento no está habilitada esta funcionalidad..

- **¿Se puede modificar una campaña, de la cual aún no se ha efectuado envío (envío programado)?**

  Si se puede modificar

- **¿Se puede modificar una campana en medio de un envió?**

  No se puede modificar

- **¿Puedo personalizar los reportes que recibo por email?**

  Por el momento no, los reportes son iguales para todos los usuarios. 

