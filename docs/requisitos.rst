Requisitos
========

La aplicación es soportada actualmente por la siguiente lista de navegadores de Internet:

- Firefox 16.0.2+

  .. image:: img/portabilidad/firefox.png 

- Chrome 23.0.1271+

  .. image:: img/portabilidad/chrome.png 

- Opera 12.10+

  .. image:: img/portabilidad/opera.png 

- Safari 5.1.7+ 

  .. image:: img/portabilidad/safari.png 

- Internet Explorer 9+  

  .. image:: img/portabilidad/ie.png 

