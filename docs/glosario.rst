Glosario
========

**Campaña**
  Una campaña consiste en un único envió de correo electrónico a N listas de suscripción o a N sublistas de suscriptores (segmentos), por tanto la campaña tiene un ciclo de vida corto. Una campaña consta de: 
   * Asunto
   * Destinatarios
   * Cuerpo  formato de html o texto plano (contenido del mensaje)
   * Fecha de envío

**Estado subcriptores**
  Permite conocer el estado de un suscriptor así: 
   * *Suscrito:* El suscriptor se encuentra dentro de la lista
   * *Retirado:* El suscriptor se retiro de la lista
   * *Eliminado:*  El suscriptor fue retirado de la lista por el administrador de SC Mail

**Etiqueta**
  Las etiquetas o tags permiten usar en el correo electrónico anotaciones con el formato ``${TAG’X’}``, cada anotación está relacionada con un único dato del suscriptor. Se usan para que al momento de crear el correo electrónico estás sean reemplazadas por el dato del usuario, es decir, si ``${TAG1}`` está relacionado con el nombre del suscriptor y el correo es: “Buenas tardes ``${TAG1}``”, entonces cuando se cree el correo ``${TAG1}`` será reemplazado por el nombre del suscriptor, dando como resultado algo similar a: ‘’Buenas tardes Cristian”, para el caso en que el suscriptor se llame Cristian.

**Número de abiertos** 
  Estadística que cuenta el total de veces en abrirse un elemento. Aplica para correos electrónicos y enlaces dentro de correos electrónicos. ie. para esta estadística si un correo electronico en específico se ha abierto ocho veces, se cuenta ocho veces.

**Número de clic únicos** 
  Estadística que cuenta una sola apertura por elemento. Aplica para correos electrónicos y enlaces dentro de correos electrónicos, ie. para esta estadística si un correo electronico en específico se ha abierto ocho veces solo se cuenta una vez.

**Segmento**
  Es una sublista de una la lista de suscriptores. Esta sublista cumple ciertos criterios personalizables, como que estos contactos sean de cierta ciudad o profesión, No tengan segundo nombre, esposa o hijos etc


**Subscriptor**
  Es una persona que hace parte de un listado de contactos. Hace parte de la lista porque fue importado en un csv o xlsx por medio de la herramienta de eMail o porque el mismo se susbscribio por medio de un formulario embebido.




